package com.example.fragmentradiogroup;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MainActivity extends FragmentActivity {
	private final String TAG = "MainActivity";

	private RadioGroup mRadioGroup;
	private Fragment1 mF1;
	private Fragment2 mF2;
	private Fragment3 mF3;

	private FragmentTransaction transaction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		init();
		setupWidgets();
	}

	private void init() {
		transaction = getSupportFragmentManager().beginTransaction();
		if (null == mF1) {
			mF1 = new Fragment1();
		}
		transaction.add(R.id.fragment_container, mF1);
		transaction.commit();
	}

	private void setupWidgets() {

		mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.radio0:
					Log.v(TAG, "setupWidgets():radio0 clicked");
					if (null == mF1) {
						Log.v(TAG, "new Fragment1");
						mF1 = new Fragment1();
					}
					transaction = getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.fragment_container, mF1);
					transaction.commit();
					break;
				case R.id.radio1:
					Log.v(TAG, "setupWidgets():radio1 clicked");
					if (null == mF2) {
						Log.v(TAG, "new Fragment2");
						mF2 = new Fragment2();
					}
					transaction = getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.fragment_container, mF2);
					transaction.commit();
					break;
				case R.id.radio2:
					Log.v(TAG, "setupWidgets():radio2 clicked");

					if (null == mF3) {
						Log.v(TAG, "new Fragment3");
						mF3 = new Fragment3();
					}
					transaction = getSupportFragmentManager().beginTransaction();
					transaction.replace(R.id.fragment_container, mF3);
					transaction.commit();
					break;

				default:
					break;
				}
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// dataEncapsulation.closeDataBase_speedDial();
	}
}
