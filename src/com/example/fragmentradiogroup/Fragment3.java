package com.example.fragmentradiogroup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 *   类说明
 * 
 *   @creator         xldc 497937995@qq.com
 *   @create-time     2014年10月25日   上午11:45:23   
 */

public class Fragment3 extends Fragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		System.out.println("Fragment3===========>onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		System.out.println("Fragment3===========>onCreateView");

		return inflater.inflate(R.layout.fragment3, null);
	}
}
